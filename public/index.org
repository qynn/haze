#+title:  Haze
#+author: 2ynn
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css" />
#+OPTIONS: toc:nil html-postamble:nil

#+BEGIN_VERSE

  this is how it must feel to have a voice
  to have something to say
  now the feeling is gone
  like an object defined as invisible
  sitting there. stateless
  silently using resources here and there
 (but not here)
  some say memory is expandable
  
  i read a new book every other day
 (crude overstatement)
  depends on how you define the other
  take naps everyday (i don't)
  or rather naps take me out
  haze feels almost fluent
  at least there are fewer expectations
  
  a song humming from the other room
  poor taste seems to travel better downwards
  noisy tapers whispering
  hard to tell, if anything at all
  obliterating judgment from within the cracks
  between the real (the unknown)
  and the pervasive
  my new witch now talking through me
  is this how it feels to be present?
 (it hurts)
  
  some program compiling through my head
  hard to keep track
  or make sense of it all
  like trying to grab onto an image
  when you have no mind's eye
  the more you latch on to it
  the faster it escapes
  do i get to have a mind's brain instead?
  
  or maybe it just fades out
  opacity minus equal x
  someone (somewhere)
  must be tweaking that property
  object orientation feels very messy to me
  not my paradigm
  sure co-parenting variables sounds nice
 (on paper)
  shared responsibilities and all that
  ultimately though it's about commitment
  not to say it's boiling down
  
  fear is blanketing me
  like an adjective
  i make up my own void inside
  spaces or tabs?
  micro management. not lean
  hatred turns into solid time consumption
  i eat belatedness for dinner
  the one i skip consistently
  
  breathing guilt
  day in  | day out
  shame clogging my pores
  cause hyperstaticity feels safe
  leftover pain spilling out of my ears
  too stale to even stain anything
  is this how it feels to be empty?
  
  from where i sit these look parallel
  but i know they aren't and it bugs me
  immensely
  wish anyone else would notice
 (yet also not)
  acumen sounds too religious
  but it's a good excuse
  expect a new hole four days from now
  PS: i don't do renos on week days
  
  anything i do i want more of
  it's through the transitions i wade
  awkwardly
  change is not my cup of tea
  gives me a preemptive mud aftertaste
  and so does entertainment
  scary pleasure pit
  jumping in with both feet
 (tied)
  into endless mindlessness
  
  work keeps me grounded
 (or so i thought)
  into someone else's dream
  their own fear of the abyss
  but there too is only trash
  illusory residues at most
  finite brain juice
  i.e. countable
  is this how shit tastes?
  
  holding space for maybe
  case of:
       just fine
       nothing
  i feel behind my life
  in front of a computer
  forgot <Ctrl+R> used to redo
  do undo trees also communicate?
 (through their root nervous system)
  nature is something isn't it
  she keeps impressing me
  
  2ynn
 [[https://www.gitlab.com/qynn/haze][(gitlab.com/qynn)]]
#+END_VERSE


